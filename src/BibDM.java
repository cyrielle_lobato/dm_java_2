import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{



    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }




    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){

        if (liste.size()==0){  //si la liste est vide on retourne null
            return null;
        }

        int minimum = liste.get(0);    //le minimum est le premier élément de la liste
        for (Integer elem : liste){   //pour chaque élément de la liste
            if (elem < minimum)       //si l'élément est < au minimum actuel
            minimum = elem;           //le minimum devient le nouvel élément
        }
        return minimum;
    }



    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){


        for (T elem : liste){                 //pour chaque élément de la liste
            if (valeur.compareTo(elem) >= 0)  //si la valeur choisie est suppérieur à l'élément actuel de la liste
                return false;                 //on retourne faux sinon vrai
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){

        List<T> listeElemCommuns = new ArrayList<>();

        int i = 0;
        int j = 0;
        T dernierElem = null;

        while (i<liste1.size() && j<liste2.size()){               //tant que les listes ne sont pas parcourues par i ou j
            if (liste1.get(i).compareTo(liste2.get(j))<0)         //si l'élément de la liste1 < l'élément de la liste2
                i++;                                              //on incrémente i
            else if(liste1.get(i).compareTo(liste2.get(j))>0)     //sinon si  l'élément de la liste1 > l'élément de la liste2
                j++;                                              //on incrémente j
            else{
                if (liste1.get(i) != dernierElem){                //sinon si l'élément est différent du dernier element
                    listeElemCommuns.add(liste1.get(i));          //alors on ajoute
                    dernierElem = liste1.get(i);
                }
                    i++;
                    j++;
                }
            }
        return listeElemCommuns;
    }




    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){

        List<String> listeDeMots = new ArrayList<>();  //création nouvelle liste contenant des mots
        String[] texteDecoupe = texte.split(" ");      //un espace entre chaque mot (l'espace fait la coupure entre 2 mots)

        for (int i = 0; i<texteDecoupe.length; i++){
            if ( !texteDecoupe[i].equals("") ){          //si l'élément n'est pas égal au mot vide alors
                listeDeMots.add(texteDecoupe[i]);      //on ajoute le mot dans la liste
            }
        }
        return listeDeMots;
    }




    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        return null;

    }



    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;                                 //compteur de parenthèses

        for (int i = 0; i < chaine.length(); i++)
        {
            if (chaine.charAt(i) == '('){            //si on a une parenthèse ouverte
                cpt++;                               // on doit fermer cette parenthèse donc on ajoute 1 au compteur
            }
            else if (chaine.charAt(i) == ')')        //sinon si on a une parenthèse fermée
                cpt--;                               //on a bien fermé la parenthèse donc on enlève 1 au compteur
            if (cpt < 0)
                return false;
        }
        if (cpt == 0)
          return true;
        else
          return false;
    }



    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors que ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> listeParentCroch = new ArrayList<>();        //on créé une liste
        char caractere;

        for (int i = 0; i < chaine.length(); i++)
        {
            caractere = chaine.charAt(i);
            if (caractere == '(' || caractere == '['){            //si on a une parenthèse ou un crochet on l'ajoute dans la liste
              listeParentCroch.add(caractere);
            }
            else if (caractere == ']'){
              if (listeParentCroch.size() == 0 || !listeParentCroch.get(listeParentCroch.size()-1).equals('[')){
                return false;                                     //si on a un crochet fermant et que l'on a pas d'ouvrant alors c'est faux
              }
              else{
                listeParentCroch.remove(listeParentCroch.size() -1);    //on supprime l'élément ajouté dans la liste
              }
            }
            else if (caractere == ')'){
              if(listeParentCroch.size()==0 || !listeParentCroch.get(listeParentCroch.size()-1).equals('(')){
                return false;                                     //si on a une parenthèse fermante et que l'on a pas d'ouvrante alors c'est faux
              }
              else{
                listeParentCroch.remove(listeParentCroch.size()-1);     //on supprime l'élément ajouté dans la liste
              }
            }
          }
          return listeParentCroch.size()==0;            //si la liste est vide alors c'est correct
        }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){

     if (liste.size() == 0)                             //si la liste est vide on retourne false
            return false;

        int debut = 0;
        int fin = liste.size() - 1;


        while (debut <= fin){                           //tant qu'on arrive pas à la fin de la liste
            int milieu = (debut + fin) / 2;                 //on prend le milieu de la liste

            if (liste.get(milieu) < valeur){            //si la valeur est dans la partie haute de la liste alors on élimine la première partie
                debut = milieu + 1;                     //et le début devient le milieu prècédent
            }

            else if (liste.get(milieu) > valeur){        //sinon si la valeur est dans la première partie on élimine la partie la plus haute
                fin = milieu - 1;}

            else
                return true;
        }
        return false;
    }



}
